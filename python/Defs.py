from enum import IntEnum

class Color(IntEnum):
  Black=0
  White=1
  Red=2
  Green=3
  Blue=4
  Yellow=5

class Orientation(IntEnum):
  North=0
  East=1
  South=2
  West=3
