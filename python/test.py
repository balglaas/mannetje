import Defs
from Mannetje import Mannetje
from Point import Point
from OutputBase import OutputBase
#from OutputJson import OutputJson
from OutputSVG import OutputSVG

mannetjes=[]
colors=[Defs.Color.Red,Defs.Color.Green,Defs.Color.Blue,Defs.Color.Yellow]
orientations=[Defs.Orientation.North,Defs.Orientation.South]
for i in range(25):
  for j in range(25):
    mannetje=Mannetje()
    mannetje.Location=Point(30*i+50*(i%2),40*j+40*(i%2)) # eigenlijk moet dit in de output-class!
    mannetje.Color=colors[2*(i%2)+j%2]
    mannetje.Orientation=orientations[i%2]
    mannetjes.append(mannetje)
#output=OutputBase(mannetjes)
#output.Output()
#output=OutputJson(mannetjes)
#output.Output()
output=OutputSVG(mannetjes)
output.Output()
