import xml.dom.minidom
import Defs
from Mannetje import Mannetje
from OutputBase import OutputBase
from Point import Point
from Rotate import Rotate

class OutputSVG(OutputBase):
  def GetColorValue(self,color):
    colorvalue="#"
    if color==Defs.Color.Black:
      colorvalue+="000000"
    elif color==Defs.Color.White:
      colorvalue+="ffffff"
    elif color==Defs.Color.Red:
      colorvalue+="d00000"
    elif color==Defs.Color.Green:
      colorvalue+="00d000"
    elif color==Defs.Color.Blue:
      colorvalue+="0000d0"
    elif color==Defs.Color.Yellow:
      colorvalue+="e0e000"
    return(colorvalue)

  def Initialize(self):
    self.Data=[]
    self.Deltas=[]
    self.Deltas.append(Point(10,0));
    self.Deltas.append(Point(0,10));
    self.Deltas.append(Point(20,0));
    self.Deltas.append(Point(0,10));
    self.Deltas.append(Point(-20,0));
    self.Deltas.append(Point(0,10));
    self.Deltas.append(Point(10,0));
    self.Deltas.append(Point(0,20));
    self.Deltas.append(Point(-10,0));
    self.Deltas.append(Point(0,-10));
    self.Deltas.append(Point(-10,0));
    self.Deltas.append(Point(0,10));
    self.Deltas.append(Point(-10,0));
    self.Deltas.append(Point(0,-20));
    self.Deltas.append(Point(10,0));
    self.Deltas.append(Point(0,-10));
    self.Deltas.append(Point(-20,0));
    self.Deltas.append(Point(0,-10));
    self.Deltas.append(Point(20,0));

  def Process(self):
    self.XMLDoc=xml.dom.minidom.Document()
    rootnode=self.XMLDoc.createElement("svg")
    rootnode.setAttribute("xmlns:dc","http://purl.org/dc/elements/1.1/")
    rootnode.setAttribute("xmlns:cc","http://creativecommons.org/ns#")
    rootnode.setAttribute("xmlns:rdf","http://www.w3.org/1999/02/22-rdf-syntax-ns#")
    rootnode.setAttribute("xmlns:svg","http://www.w3.org/2000/svg")
    rootnode.setAttribute("xmlns","http://www.w3.org/2000/svg")
    rootnode.setAttribute("xmlns:sodipodi","http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd")
    rootnode.setAttribute("xmlns:inkscape","http://www.inkscape.org/namespaces/inkscape");
    rootnode.setAttribute("width","210mm");
    rootnode.setAttribute("height","297mm");
    rootnode.setAttribute("version","1.1");
    rootnode.setAttribute("id","svg8");
    self.XMLDoc.appendChild(rootnode)
    gnode=self.XMLDoc.createElement("g");
    gnode.setAttribute("inkscape:label","Layer 1")
    gnode.setAttribute("inkscape:groupmode","layer")
    gnode.setAttribute("id","layer1")
    rootnode.appendChild(gnode)
    id=0
    for mannetje in self.Mannetjes:
      pathnode=self.XMLDoc.createElement("path");
      pathnode.setAttribute("style","fill:%s;fill-opacity:1;stroke:%s;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"% (self.GetColorValue(mannetje.Color),(self.GetColorValue(mannetje.Border))))
      rotate=Rotate(mannetje.Orientation)
      start=rotate.Rotate(Point(20,0))
      x1=mannetje.Location.X1+start.X1
      x2=mannetje.Location.X2+start.X2
      #path="M %d,%d" % (start.X1+mannetje.Location.X1,start.X2+mannetje.Location.X2)
      path="M %d,%d" % (x1,x2)
      for delta in self.Deltas:
        rotated=rotate.Rotate(delta)
        x1+=rotated.X1
        x2+=rotated.X2
        path+=" %d,%d" % (x1,x2)
      path+=" z"
      pathnode.setAttribute("d",path);
      pathnode.setAttribute("id","mannetje%05d" % (id))
      id+=1
      pathnode.setAttribute("inkscape:connector-curvature","0")
      pathnode.setAttribute("sodipodi:nodetypes","ccccccccccccccccccccc")
      gnode.appendChild(pathnode)
      """
      d["Location"]={}
      d["Location"]["X1"]=mannetje.Location.X1
      d["Location"]["X2"]=mannetje.Location.X2
      d["Color"]=int(mannetje.Color)
      d["Orientation"]=int(mannetje.Orientation)
      self.Data.append(d)
      """

  def Finalize(self):
    #print json.dumps(self.Data)
    #with open("mannetjes.json", "w") as f:
    fp_out=open("mannetjes.svg","w")
    fp_out.write(str(self.XMLDoc.toxml("utf-8")))
    fp_out.close()


