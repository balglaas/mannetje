import json
from Mannetje import Mannetje
from OutputBase import OutputBase
from Point import Point

class OutputJson(OutputBase):

  def Initialize(self):
    self.Data=[]

  def Process(self):
    for mannetje in self.Mannetjes:
      d={}
      d["Location"]={}
      d["Location"]["X1"]=mannetje.Location.X1
      d["Location"]["X2"]=mannetje.Location.X2
      d["Color"]=int(mannetje.Color)
      d["Orientation"]=int(mannetje.Orientation)
      self.Data.append(d)

  def Finalize(self):
    #print json.dumps(self.Data)
    with open("mannetjes.json", "w") as f:
      json.dump(self.Data, f)

