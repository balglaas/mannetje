import Defs
from Point import Point
"""
cos -sin
sin  cos
"""

class Rotate:
  def __init__(self,orientation):
    self.Orientation=orientation

  def Rotate(self,point):
    if self.Orientation==Defs.Orientation.North:
      return(point)
    x1=point.X1
    x2=point.X2
    if self.Orientation==Defs.Orientation.South:
      x1=-x1
      x2=-x2
    #elif orientation=Defs.Orientation.East:
    #elif orientation=Defs.Orientation.West:
    return(Point(x1,x2))
