from Mannetje import Mannetje
from Point import Point

class OutputBase():
  def __init__(self,mannetjes):
    self.Mannetjes=mannetjes

  def Initialize(self):
    print("Initialize")

  def Process(self):
    for mannetje in self.Mannetjes:
      print(mannetje.Location.X1,mannetje.Location.X2,mannetje.Color,mannetje.Orientation)

  def Finalize(self):
    print("Finalize")

  def Output(self):
    self.Initialize()
    self.Process()
    self.Finalize()
